<?php

/**
 * @file Define.class.php
 * @brief 共通定数定義
 * 共通定数を定義する
 * @author tac@goodbee.jp
 * @date 2010/02
 * @version 1.0
 *
 */

require_once "CommonDefinition.php";

/**
 * @class Define
 * 定数管理クラス
 *
*/
class Definition extends CommonDefinition
{
	/****************************************/
	/* 										*/
	/****************************************/
}

?>