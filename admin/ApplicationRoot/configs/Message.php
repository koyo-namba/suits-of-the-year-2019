<?php

/**
 * @file Message.class.php
 * @brief メッセージ管理
 * メッセージを一括管理する
 * @author tac@goodbee.jp
 * @date 2010/02
 * @version 1.0
 *
 */

require_once 'CommonMessage.php';

/**
 * @class メッセージ管理クラス
 * メッセージを一括管理する
 *
 */
class Message extends CommonMessage
{

	/****************************************/
	/* タイトル								*/
	/****************************************/
	const TITLE_INFO						= "インフォメーション";
	const TITLE_ERROR						= "エラー";
	const TITLE_EXCPT_ERROR					= "例外エラー";
	/****************************************/
	/* システムエラー						*/
	/****************************************/
	const MSG_SEND_MAIL_NG					= 'メールの送信に失敗しました。';
}

?>
