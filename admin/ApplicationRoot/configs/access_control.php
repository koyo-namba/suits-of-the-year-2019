<?php
$access_control_config = array(


	// ログインせずにアクセス可能なコントロールの一覧
	'no_login_allow' => array(
		// 設定値は、適宜追加します。わかった人がしてくれてもOK
		'error',
		'login',
		'survey',
		'entry',
	),


	// ログインせずにアクセス可能なコントロール=>アクションの一覧
	'action_auth_tbl' => array(
		'controller'=>'action',
	),

	// ログインユーザの権限によりアクセス許可するコントロールの一覧
	// 全権限で許可する場合は記述省略可能
	'auth_tbl' => array(
		// 設定値は、適宜追加します。わかった人がしてくれてもOK

	),


	// ★★★★★★★★★ 本番時は必ず、以下をfalse、または全体をコメントアウトする事 ★★★★★★★★★
	'debug_dummy_login' => false,
	'dummy_user' => array(
		'user_id' => '1',
		'user_code' => 'test',
		'kind' => '20',
		'user_name' => 'ダミーユーザ',
		'company_name' => '株式会社XXXX'
	),

);

