<?php


require_once 'framework/ControllerAbstract.php';
require_once 'helper/SessionManager.php';
require_once 'Exception.php';

class LoginController extends Framework_ControllerAbstract
{
	protected function _getValidationSetting() {
		// key = action-name, var = { validation-ini-file, section-name }
		return array('login' => array('validation_user.ini', 'login'));
	}

	public function indexAction()
	{
		@SessionManager::logout(); // 未セッション時のWarning抑止

		$this->_render('Login');
	}

	public function loginAction()
	{
		// ログイン
		$where = array(
			"user_id" => $this->_getParam('user_id'),
			"password" => $this->_getParam('password')
		);

		// $where = array(
		// 	"user_id" => $this->_getParam('user_id'),
		// 	"password" => $this->_getParam('password')
		// );


		//ログイン情報の取得
		$user = $this->_query(null, 'sample_sql', $where, false);

		// レコード有無チェック→無かったらException
		// $user = Functions::selectFrom($this->_getDBh(),'m_admin',$where);




		// ダミーデータ
		// $user = array(
		// 	array('user_id'=>1, 'password'=>'password')
		// );

		if(count($user) > 0){
			// ログイン
			SessionManager::login($user[0]);

			$this->_forward('index', 'top', null, array());
		}
		else{
			// エラー
			$err_msg[] = ErrMessage::create('1', 'ログイン', 'IDまたはパスワードが間違っています。', '', '');
			$this->_smarty->assign('err_msg', $err_msg);
			$this->_rerender();
			return;
		}
	}

}