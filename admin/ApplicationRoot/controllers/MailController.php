<?php



require_once 'framework/ControllerAbstract.php';
require_once 'helper/SessionManager.php';
require_once 'Exception.php';
require_once 'CommonFunction.php';
require_once 'helper/Mail.php';


class MailController extends Framework_ControllerAbstract
{
	//Mail送信TOPに移動
	public function mailtopAction()
	{
		$this->_render('MailTop');
	}
	
	//Mail送信確認画面に移行
	public function winnermailconfirmAction()
	{
		$this->confirmMail();
		$this->_render('winnerMailconfirm');
	}
	public function remindmailconfirmAction()
	{
		$this->confirmMail();
		$this->_render('remindMailconfirm');
	}
	public function surveymailconfirmAction()
	{
		$this->confirmMail();
		$this->_render('surveyMailconfirm');
	}

	//当選メール送信
	public function winnermailAction()
	{
		//メール送信
		$this->sendMail("winner_mail.tpl", "winners_flag");
		//送信完了画面に移行
		$this->_render('winnermailend');
	}

	//リマインドメール送信
	public function remindmailAction()
	{
		//メール送信
		$this->sendMail("remind_mail.tpl", "winners_flag");
		//送信完了画面に移行
		$this->_render('remindmailend');
	}

	//アンケートメール送信
	public function surveymailAction()
	{
		$where = array();
		//DBのデータを連想配列で取得
		$visitors = Functions::selectFrom($this->_getDBh(), 'entries', $where);

		for($i = 0 ; $i <= count($visitors) -1 ; $i++ ){
			$where = array(
				"flag" => $visitors[$i]["visitors_flag"],
				"email" => $visitors[$i]["email"],
				"kanji_sei" => $visitors[$i]["kanji_sei"],
				"kanji_mei" => $visitors[$i]["kanji_mei"],
				"kana_sei" => $visitors[$i]["kana_sei"],
				"kana_mei" => $visitors[$i]["kana_mei"],
				"access_token" => $visitors[$i]["access_token"],
			);
			//もしフラグがたっていたらMail送信
			if($where['flag'] == 1){
				$this->_smarty->assign('where', $where);
				require(MAIL_TPL_DIR."survey_mail.tpl");
							  //宛先のみ記載
				Mail::send($where['email'], $title, $content, $from);
			}
		}

		$this->_render('surveymailend');
	}

	//当選メール、リマインドメール送信関数
	private function sendMail($tpl_name, $flag_name){
		$where = array();
		//DBのデータを連想配列で取得
		$winner = Functions::selectFrom($this->_getDBh(), 'entries', $where);
		for($i = 0; $i <= count($winner) -1 ; $i++ ){
			$where = array(
				"flag" => $winner[$i][$flag_name],
				"email" => $winner[$i]["email"],
				"kanji_sei" => $winner[$i]["kanji_sei"],
				"kanji_mei" => $winner[$i]["kanji_mei"],
				"kana_sei" => $winner[$i]["kana_sei"],
				"kana_mei" => $winner[$i]["kana_mei"],
			);
			//もしフラグがたっていたらMail送信
			if($where['flag'] == 1){
				$this->_smarty->assign('where', $where);
				require(MAIL_TPL_DIR.$tpl_name);
				//宛先のみ記載
				Mail::send($where['email'], $title, $content, $from);
			}
		}
	}
	
	//当選メール、リマインドメール送信確認関数
	private function confirmMail(){
		$where = array();
		//DBのデータを連想配列で取得
		$winner = Functions::selectFrom($this->_getDBh(), 'entries', $where);
		$counts = count($winner);
		$this->_smarty->assign("winner", $winner);
		$this->_smarty->assign("counts", $counts);
	}
}




