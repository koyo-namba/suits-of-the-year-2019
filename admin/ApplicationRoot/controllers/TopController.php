<?php



require_once 'framework/ControllerAbstract.php';
require_once 'helper/SessionManager.php';
require_once 'Exception.php';
require_once 'CommonFunction.php';
require_once 'helper/Mail.php';


class TopController extends Framework_ControllerAbstract
{
	protected function _getValidationSetting() {
	}

	//TOP画面に移行
	//最初の画面は応募者一覧
	public function indexAction()
	{
		$where = array();
		//DBのデータを連想配列で取得
		$entries = Functions::selectFrom($this->_getDBh(), 'entries', $where, array("create_date desc"));
		//flagを〇に変更
		for($i=0; $i<count($entries); $i++){
			if($entries[$i]["winners_flag"] == "1"){
				$entries[$i]["winners_flag"] = "〇";
			}elseif($entries[$i]["winners_flag"] == "0"){
				$entries[$i]["winners_flag"] = "";
			}
			if($entries[$i]["visitors_flag"] == "1"){
				$entries[$i]["visitors_flag"] = "〇";
			}elseif($entries[$i]["visitors_flag"] == "0"){
				$entries[$i]["visitors_flag"] = "";
			}
			if($entries[$i]["gest_age"]  !== ""){
				$entries[$i]["gest_age"] = "〇";
			}
		}

		//現在の応募数を計算
		$mass = count($entries);
		//templetsへの挿入
		$this->_smarty->assign('entries', $entries);
		$this->_smarty->assign('mass', $mass);
		$this->_render('Top');
	}
}




