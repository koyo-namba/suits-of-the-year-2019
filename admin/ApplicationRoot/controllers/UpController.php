<?php



require_once 'framework/ControllerAbstract.php';
require_once 'helper/SessionManager.php';
require_once 'Exception.php';
require_once 'CommonFunction.php';
require_once 'helper/Mail.php';
//PHPExcel読み込み
require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/IOFactory.php';


class UpController extends Framework_ControllerAbstract
{
	//当選者リストアップロード画面
	public function winnerupAction()
	{
		$this->_render('Winnerup');
	}

	//当選者リストアップロード関数
	public function winnerflagupAction()
	{
		//ExcelをUpload
		$this->excelUp('winners_upload', 'winners_flag');
		//完了画面に移行
		$this->_render('Winnerupend');
	}
	
	//来場者リストアップロード画面
	public function visitorsupAction()
    {
		$this->_render('Visitorsup');
	}

	//来場者リストアップロード関数
	public function visitorsflagupAction()
	{
		//ExcelをUpload
		$this->excelUp('visitors_upload', 'visitors_flag');
		//完了画面に移行
		$this->_render('Visitorsupend');
	}

	//token作成関数（https://www.websec-room.com/2013/03/05/443）に書いてある
	private function get_access_token() {
		$length = 16;//16*2=32バイト
		$bytes = openssl_random_pseudo_bytes($length);
		return bin2hex($bytes);
	}

	//ExcelのUP関数
	private function excelUp($file_name_load, $flag_name){
		//flagをnullにする
		$where = array();
		//entriesの全データを配列化
		$all_entries_date = Functions::selectFrom($this->_getDBh(), 'entries', $where);
		for($i = 0; $i <= count($all_entries_date); $i++ ){
			//entriesのemailをkeyにする
			$where = array(
				"email" => $all_entries_date[$i]['email'],
			);
			//entriesテーブルのflagをnullにする
			Functions::updateTo($this->_getDBh(), 'entries', array($flag_name => null, 'access_token' => null), $where, 'root');
		}
		//アップデートしたファイルの保存先
		$upload = './up_file/'.$_FILES[$file_name_load]['name'];
		//アップロードが正しく完了したかチェック
		if(move_uploaded_file($_FILES[$file_name_load]['tmp_name'], $upload)){
			//アップロートしたファイル名
			$up_file = $_FILES[$file_name_load]['name'];
			//読込対象のエクセルの指定
			$objPExcel = PHPExcel_IOFactory::load("./up_file/$up_file");
			//読み込んだエクセルの配列化
			$list = $objPExcel->getActiveSheet()->toArray(null, true, true, true);
			//タイトルスキップ
			unset($list[1]);
			//読み込んだExcelの配列を回す
			for($i = 1; $i <= count($list) + 1; $i++ ){
				if($i == 1){
					continue;
				}
				//access_token作成
				$access_token = $this->get_access_token();
				//ExcelのEmailのみ取り出す-送られてきたExcelによって$list[$i]["O"]を変える
				$where = array(
					"email" => $list[$i]["O"],
				);
				//entriesテーブルのvisitors_flagにフラグを立てる + access_token挿入
				Functions::updateTo($this->_getDBh(), 'entries', array($flag_name => '1', 'access_token' => $access_token), $where, 'root');
			}
		}else{
			echo 'アップロード失敗';exit;
		}
	}

}




