<?php
require_once 'Zend/Config/Ini.php';

define('IMG_TEMPLATE_DIR', DOC_ROOT.'/catalog/template/');

class AppConfig
{
	public static $config;

	public static function initialize($ini_file, $section) {
		AppConfig::$config = new Zend_Config_Ini($ini_file, $section);
		AppConfig::$config->setReadOnly();
	}

	private function __construct() {
		//
	}

	public static function get($name, $default = null) {
		return AppConfig::$config->get($name, $default);
	}

	public static function toArray() {
		return AppConfig::$config->toArray();
	}
}