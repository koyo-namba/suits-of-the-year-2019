<?php

/**
 * @file AdminCommon.class.php
 * @brief 管理システム共通関数定義
 * 管理システム共通関数を定義する
 * @author tac@goodbee.jp
 * @date 2010/09
 * @version 1.0
 *
 */


require_once 'CommonFunction.php';
require_once 'ErrMessage.php';


class Functions extends CommonFunction
{
	private static $tsv_tpl_dir;
	private static $temp_tsv_dir;

	public static function initialize(array $path_args) {
		parent::initialize($path_args);

		//check
		$keys = array_keys($path_args);
		if (!in_array('tsv_tpl_dir', $keys)
		|| !in_array('temp_tsv_dir', $keys)) {
			throw new Exception("[FW] Failed to construct Functions.");
		}

		Functions::$tsv_tpl_dir = $path_args['tsv_tpl_dir'];
		Functions::$temp_tsv_dir = $path_args['temp_tsv_dir'];
	}

	// singleton
	private function __construct(){
		// enable to create instance
	}


	/**
	 * ランダム文字列生成
	 * @param $base 基本文字列
	 * @return $result ランダム文字列
	 */
	public static function getRandomString($base){
		$input = $base.date("YmdH:i:s");
		$result = md5($input);
	    return $result;
	}


	/** 表示用一覧取得
	 * 表示用一覧を取得する。
	 * @retval list 一覧
	 */
//	function createDispList($list, $cnt)
//	{
//		if($cnt > 0){
//			$cur = Common::getPageNo()-1;
//			$start = $cur*DefineClass::PAGER_LIST_MAX;
//			for($i=$start; $i<$start+DefineClass::PAGER_LIST_MAX; $i++){
//				if($i>=$cnt) break;
//				$list_disp[$i] = $list[$i];
//			}
//		}
//		return $list_disp;
//	}
	public static function createDispList($list, $cnt, $page_no, $list_max)
	{
		if($cnt > 0){
			$cur = $page_no-1;
			$start = $cur*$list_max;
			for($i=$start; $i<$start+$list_max; $i++){
				if($i>=$cnt) break;
				$list_disp[$i] = $list[$i];
			}
		}
		return $list_disp;
	}
	public static function createDispList2($list, $cnt, $page_no, $list_max)
	{
		if($cnt > 0){
			$cur = $page_no-1;
			$start = $cur*$list_max;
			for($i=$start; $i<$start+$list_max; $i++){
				if($i>=$cnt) break;
				$list_disp[$i-$start] = $list[$i];
			}
		}
		return $list_disp;
	}


	/**
	 * 画像パスを取得（宿泊）
	 * @param $list    エリアID、meny_noを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getImageFilePathStay($list){
		return IMG_STAY_DIR_URL.$list['area_large_id'].'/'.$list['area_mid_id'].'/'.$list['area_small_id'].'/'.$list['menu_no'].'/';
	}

	/**
	 * 画像フォルダの絶対パスを取得（宿泊）
	 * @param $list    エリアID、meny_noを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getImageFileRealPathStay($list){
		return IMG_STAY_DIR.$list['area_large_id'].'/'.$list['area_mid_id'].'/'.$list['area_small_id'].'/'.$list['menu_no'].'/';
	}

	/**
	 * 小エリアまでの絶対パスを取得（宿泊）
	 * @param $list    エリアIDを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getSmallAreaRealPathStay($list){
		return IMG_STAY_DIR.$list['area_large_id'].'/'.$list['area_mid_id'].'/'.$list['area_small_id'].'/';
	}

	/**
	 * IDサーバ側の画像フォルダの絶対パスを取得（宿泊）
	 * @param $list    エリアID、meny_noを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getMntRealPathStay($list){
		return AppConfig::get('ids_mnt_path').'stay/'.$list['area_large_id'].'/'.$list['area_mid_id'].'/'.$list['area_small_id'].'/'.$list['menu_no'].'/';
	}

	/**
	 * 小エリアまでの絶対パスを取得（宿泊）
	 * @param $list    エリアIDを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getMntSmallAreaRealPathStay($list){
		return AppConfig::get('ids_mnt_path').'stay/'.$list['area_large_id'].'/'.$list['area_mid_id'].'/'.$list['area_small_id'].'/';
	}




/******* menu_noだけをもとにフォルダを作る場合に使用(サービス) *******/

	/**
	 * 画像パスを取得（サービス）
	 * @param $list    meny_noを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getImageFilePathService($list){
		return IMG_SERVICE_DIR_URL.$list['menu_no'].'/';
	}

	/**
	 * 画像フォルダの絶対パスを取得（サービス）
	 * @param $list    meny_noを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getImageFileRealPathService($list){
		return IMG_SERVICE_DIR.$list['menu_no'].'/';
	}

	/**
	 * IDサーバ側の画像フォルダの絶対パスを取得（サービス）
	 * @param $list    meny_noを含んだ配列
	 * @return $result 画像パス
	 */
	public static function getMntRealPathService($list){
		return AppConfig::get('ids_mnt_path').'service/'.$list['menu_no'].'/';
	}
	
	



	


}