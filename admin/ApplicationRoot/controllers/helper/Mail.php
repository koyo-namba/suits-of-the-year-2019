<?php

require_once "Exception.php";
require_once 'TplFile.php';
require_once "Zend/Mail.php";
require_once "Zend/Mail/Transport/Smtp.php";

// 将来的な機能拡張を踏まえ、Zend_Mail採用
// 現状実装がとても難儀なので、あまり有用に見えないが。。。
class Mail
{

	const CONTEXT_CHARSET = 'UTF-8';
	const ZEND_MAIL_CHARSET = 'ISO-2022-JP';
	const MAIL_CHARSET = 'ISO-2022-JP-MS';
	
	public static function send($to, $title, $content, $from, $path=null, $file=null, $cc=null, $bcc=null) {
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");	// ソースの文字コードに合わせる

//		if (AppConfig::get('use_smtp') == true) {

			$mail = new Zend_Mail(Mail::ZEND_MAIL_CHARSET);
			$mail->setFrom($from[0], mb_encode_mimeheader($from[1], Mail::MAIL_CHARSET));
			
			// to
			$p_address = null;
			$p_address = explode(",", $to);
			foreach ($p_address as $val) {
				$mail->addTo(trim($val));
			}
			// cc
			if($cc!=''){
				$p_address = null;
				$p_address = explode(",", $cc);
				foreach ($p_address as $val) {
					$mail->addCc(trim($val));
				}
			}
			// bcc
			if($bcc!=''){
				$p_address = null;
				$p_address = explode(",", $bcc);
				foreach ($p_address as $val) {
					$mail->addBcc(trim($val));
				}
			}
			$enc_title = mb_encode_mimeheader($title, Mail::MAIL_CHARSET);
			$mail->setSubject($enc_title);

//			$mail->Encoding = "7bit";
			$body = mb_convert_encoding($content, Mail::MAIL_CHARSET, CONTEXT_CHARSET);
			$mail->setBodyText($body);

			if($path!=null && $file !=null)
			{
				// 添付ファイル
				$file_contents = file_get_contents($path.$file);
				$att = $mail->createAttachment($file_contents);
				$att->filename = $file; // 添付されるファイル名
			}

			if (AppConfig::get('use_smtp_auth') == true) {
				$config = array(
					'port' => AppConfig::get('smtp_port'),
					'auth' => 'login',
					'username' => AppConfig::get('smtp_user'),
					'password' => AppConfig::get('smtp_password'));
			} else {
				$config = array('port' => AppConfig::get('smtp_port'));
			}
			$smtp = new Zend_Mail_Transport_Smtp(AppConfig::get('smtp_server'), $config);

			try {
				$mail->send($smtp); // throws Zend_Mail_Transport_Exception
			} catch(Zend_Mail_Transport_Exception $e) {
				throw new SystemException(Message::MSG_SEND_MAIL_NG, '', $e);
			} catch(Zend_Mail_Protocol_Exception $e) {
				throw new SystemException(Message::MSG_SEND_MAIL_NG, '', $e);
			}

			Logger::getLogger('Mail')->debug("send mail from [{$from[0]}] to [{$to}]");

//		} else {
//
//			throw new Exception('Please use smtp');
////			mb_internal_encoding(Mail::CONTEXT_CHARSET);
////			$ret = mb_send_mail($to, $title, $content, $from);
////			if (!$ret)
////				throw new SystemException(Message::MSG_SEND_MAIL_NG);
//		}
	}

   public static function mkMailBody($tpl_dir, $tpl, $content){

        $tplfile_class = new TplFile($tpl.'.tpl', $tpl_dir);

        $tplfile_class->assign('content', $content);

        return $tplfile_class->createAsStr()->getfilestr();

    }
}