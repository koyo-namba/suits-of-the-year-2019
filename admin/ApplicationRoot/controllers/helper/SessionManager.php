<?php

require_once "AppConfig.php";
require_once "framework/SessionManagerAbstract.php";
require_once 'HtmlTagData.php';
	
session_cache_limiter('none');

class SessionManager extends Framework_SessionManagerAbstract
{

	

	public static function isNotLogin() {
		@session_start();
		return (empty($_SESSION['sesid'])
			|| $_SESSION['sesid'] == ''
			|| $_SESSION['site_id'] != AppConfig::get('site_id')
		);
	}

	public static function login($user) {
		// ログイン情報は必要に応じて追加
		$session_id = parent::startSession();
		$_SESSION['sesid']     = $session_id;				// セッションID.新規IDを割当て
		$_SESSION['user_id']     = $user['user_id'];		// ユーザーID
		$_SESSION['password']     = $user['password'];		// password
		$_SESSION['site_id'] = AppConfig::get('site_id');	// サイトID
		
		Logger::getRootLogger()->debug('LOGIN with session_id :'.$session_id);
	}

	public static function logout() {
		parent::destroySession();
		Logger::getRootLogger()->debug('LOGOUT');
	}

	public static function getUserInfo() {
		return array(
			'sesid'     => $_SESSION['sesid'],
			'site_id' => $_SESSION['site_id'],
			'user_id' => $_SESSION['user_id'],
			'password'   => $_SESSION['password'],
		);
	}

	public static function role($role) {
		if (SessionManager::isNotLogin())
			throw new Exception('Save-method failure. Session is available only after login!');

		$arr = array();
		foreach($role as $val){
			$arr[$val['role_code']] = $val['role_id'];
		}
		$_SESSION['role'] = $arr;
		var_dump($arr);
	}

	public static function saveCondition($controller, $param) {
		if (SessionManager::isNotLogin())
			throw new Exception('Save-method failure. Session is available only after login!');
		parent::saveCondition($controller, $param);
	}

	public static function saveDispdata($controller, $param) {
		if (SessionManager::isNotLogin())
			throw new Exception('Save-method failure. Session is available only after login!');
		parent::saveDispdata($controller, $param);
	}

	public static function saveTransitionParam($param) {
		if (SessionManager::isNotLogin())
			throw new Exception('Save-method failure. Session is available only after login!');
		parent::saveTransitionParam($param);
	}

	public static function setSession($key, $val) {
		$_SESSION[$key] = $val;
	}
	public static function getSession($key) {
		
		return $_SESSION[$key];
	}
	public static function setCookie($key, $val, $expire) {
		if (SessionManager::isNotLogin())
			throw new Exception('Save-method failure. Session is available only after login!');
		$ret = setcookie($key, $val, $expire, '/');
	}
	public static function getCookie($key) {
		if (SessionManager::isNotLogin())
			throw new Exception('Save-method failure. Session is available only after login!');
		return $_COOKIE[$key];
	}

	public static function startSession() {
		parent::startSession();
	}
	
	

	
}