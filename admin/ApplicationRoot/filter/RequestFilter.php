<?php
require_once 'Logger.php';
require_once 'Exception.php';
require_once 'framework/RequestFilterAbstract.php';
require_once APP_ROOT.'controllers/helper/SessionManager.php';

class Filter_RequestFilter extends Framework_RequestFilterAbstract
{

	private $_no_login_allow = NULL;
	private $_auth_control_tbl = NULL;
	private $_auth_action_tbl = NULL;

	private $_debug_dummy_login = false;
	private $_dummy_user = NULL;

	private $_logger = null;

	public function __construct() {

		// アクセス制御関連設定ファイルの読込み
		require_once APP_ROOT.'/configs/access_control.php';
		$this->_no_login_allow = $access_control_config['no_login_allow'];
		$this->_auth_control_tbl = $access_control_config['auth_tbl'];
		$this->_auth_action_tbl = $access_control_config['action_auth_tbl'];

		// Debug用オプション設定
		if (in_array('debug_dummy_login', $access_control_config))
			$this->_debug_dummy_login = $access_control_config['debug_dummy_login'];
		if (in_array('dummy_user', $access_control_config))
			$this->_dummy_user = $access_control_config['dummy_user'];

		$this->_logger = Logger::getLogger('FILTER');
	}

	protected function _checkLogin($controller, $action, $request) {

		$through = false;
        foreach($this->_auth_action_tbl as $key=>$val){
        	if($key == $controller && $val == $action){
        		$through = true;
        	}
        }

		if (in_array($controller, $this->_no_login_allow) && !$through)
        	return; // チェック対象外

        if (SessionManager::isNotLogin() && !$through) {

	        // 開発用ダミーユーザログイン機能使用時
	        if ($this->_debug_dummy_login && $this->_dummy_user != NULL) {
				SessionManager::login($this->_dummy_user);
				$this->_logger->debug('[RF] Login has done with debug user.');
				return;
	        }

        	throw new LoginException('');
        }

        //**********add ajax時にトークンチェックしない
        if(($action === 'error')
			|| (strpos($action, 'download') !== FALSE)
			|| (strpos($action, 'ajax') !== FALSE)){
				return;
			}
        //**********add

        // リクエストトーケンチェック：リロードやマルチセッションを防ぐ
		$saved_token = SessionManager::getSavedToken();
		if ($saved_token === '')
			return true;

		$token = (string)$request->getParam('request_token');
//		if ($token !== $saved_token) {
//			if ((bool)AppConfig::get('debug_token_check_off') !== true)
//				throw new TokenException("Token error: MultiSession or Reload. saved:{$saved_token}, requested:{$token}");
//
//		}
	}

	protected function _checkAuth($controller, $action) {

		// 権限チェック
        if (!in_array($controller, array_keys($this->_auth_control_tbl))) {
			$this->_logger->debug('[RF] Auth check is not required.');
        	return;
        }

        $this->_logger->debug('[RF] Auth check is required.');

        $tbl = $this->_auth_control_tbl[$controller];
        $user = SessionManager::getUserInfo();
        if (in_array($user['role_id'], $tbl)) {
			$this->_logger->debug('[RF] Auth check OK!.');
			return;
        }

        // 権限違反エラー
        throw new AuthException();
	}

}
