<?php
$from    = array(AppConfig::get('mail_from'), AppConfig::get('mail_from_name'));
$title = "【SUITS OF THE YEAR 2019】リマインドメール";

$content = "{$select_floor}SUITS OF THE YEAR 2019 事務局より


{$where['kanji_sei']} {$where['kanji_mei']} 様
({$where['kana_sei']} {$where['kana_mei']} サマ)

SUITS OF THE YEAR 2019事務局です。
この度は、スーツオブザイヤー2019へのご応募、誠にありがとうございました。

いよいよ、来週11月7日（木）スーツオブザイヤー開催となります。
皆様にお越し頂ける様に再度、ご連絡させて頂きます。


尚、開演中は報道関係者の皆様を除き、カメラ撮影、録音、録画はご遠慮頂いて
おりますのでご協力をお願いいたします。


また、イベント終了後、ご参加頂きました皆様には、次回のスーツオブザイヤーの開催が
より良くなる様にアンケートのご協力をお願いしたいと存じます。

それでは、改めて当日、会場でお会いできることスタッフ一同、楽しみにしております。



当日はこちらのご当選案内のメールが確認出来るように、ご準備の上
受付にお越し頂きスタッフへお見せください。
※確認方法：携帯画面、パソコン画面他、ご案内メールのプリントアウトでも
結構です。


受付開始時間および予定スケジュールは下記の通りとなります。
ご確認下さい。


■日程：2019年11月7日（木）
■会場：東京ミッドタウン日比谷6階 BASE Q（東京都千代田区有楽町1-1-2）
☆会場へのアクセス案内：https://www.hibiya.tokyo-midtown.com/jp/baseq/access/
■スケジュール（予定）：
　□受付開始：18時
　□メインホール開場：18時30分
　□第1部 授賞式：19時開演
　□第2部 アフターパーティー：20時開始
　□終了：21時30分
※スケジュールは当日の状況により、多少変動することがございます。
予めご了承頂ますようお願い致します。
■注意事項：会場の広さ、ご用意のお席の席数に限りがありますので
応募申込時の人数以上の方は入場をお断りさせて頂きますので予めご了承下さい。

以上です。

ご質問などございましたら、下記、事務局までお問い合わせ下さい。

それでは、当日、お会いできることスタッフ一同、楽しみにしております。


SUITS OF THE YEAR 2019 事務局
E-mail：info@suitsoftheyear.com
";
?>
