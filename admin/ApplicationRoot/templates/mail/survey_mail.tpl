<?php
$from    = array(AppConfig::get('mail_from'), AppConfig::get('mail_from_name'));
$title = "【SUITS OF THE YEAR 2019】アンケートメール";

$content = "{$select_floor}SUITS OF THE YEAR 2019 事務局より

★受付リストを元に来場頂いた方のみに送信致します

{$where['kanji_sei']} {$where['kanji_mei']} 様
({$where['kana_sei']} {$where['kana_mei']} サマ)


SUITS OF THE YEAR 2019事務局です。
昨日は、お忙しい中、スーツオブ・ザ・イヤー201９会場へご来場頂き、
誠にありがとうございました。

ご来場頂きました皆様よりご意見を頂戴して、今後の展開に活かしてまいります。
お忙しいとは存じますが、下記、アンケートフォームにてご協力いただけますと幸いです。

★http://soy.localhost/suitsoftheyear/survey?token={$where['access_token']}

今後ともよろしくお願いいたします。

SUITS OF THE YEAR 201９ 事務局
E-mail：info@suitsoftheyear.com

";
?>
