{include file="PageHeader.tpl" login='login' title='' menu=''}

<body>
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
    <h1>例外エラー</h1>
    <div class="alert alert-error alert-block">
      <h4>{$title}</h4>
      {foreach from=$messages item=msg}
      <p>{$msg nofilter}</p>
      {/foreach}
    </div>
    <p class="text-center">
      <a href="#" class="btn btn-primary" onclick="javascript:submit_c('login'); return false;"><i class="icon-remove icon-white"></i> ログアウト</a>
    </p>

    {if $smarty_debug_frag eq '1'}
    <p>DETAIL:</p>
    <p>{$detail nofilter}</p>
    <h1>stacktrace - {$class}</h1>
    <table class="table table-bordered table-striped table-hover">
      <tbody>
        {foreach from=$trace item=stack}
        <tr>
          <th>{$stack.class}{$stack.type}{$stack.function}</th>
          <td>{$stack.file}:{$stack.line}</td>
        </tr>
        {/foreach}
      </tbody>
    </table>
    {/if}
  </div>
  
{include file="PageFooter.tpl" login='login'}