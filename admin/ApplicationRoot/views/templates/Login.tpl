{include file="PageHeader.tpl"}
<body>
<!--header-->
{include file="Message.tpl"}
<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
      <div class="py-5">
      <div class="col-md-12 d-flex justify-content-center">
        <h2 class="d-inline-block position-relative title">LOGIN&nbsp;FORM</h2>
      </div>
      </div>  
      <div class="body bg-gray">
        <div class="form-group">
          <div class="col-md-12 d-flex justify-content-center">
            <div class="col-sm-6" >
              <input type="text" name="user_id" class="form-control" placeholder="USER&nbsp;ID" value="" />
            </div>
          </div>
        </div>
        <div class="col-md-12 d-flex justify-content-center">
          <div class="col-sm-6">
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="PASSWORD" />
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-lg-4 mx-auto my-5">
        <button onclick="submit_c('login', 'login'); return false" class="btn btn-primary btn-block">LOG&nbsp;IN</button>
      </div>
    </div>
  </div>
  </div>
  </form>
  <br>
</div>
{include file="PageFooter.tpl"}
{include file="PageEnd.tpl"}
