{include file="PageHeader.tpl"}
<body>
<!--header-->
{include file="Message.tpl"}
<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      {include file="PageNavi.tpl" nav="mailtop"}
      <div class="py-2">
      </div>  
      <div class="col-md-12 d-flex justify-content-center">
        <h2 class="d-inline-block position-relative title">Mail送信画面</h2>
      </div>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-0" >
      <button onclick="submit_c('mail', 'winnermailconfirm'); return false" class="btn btn-warning btn-block"><strong>当選メール送信</strong></button>
    </div>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-1" >
      <button onclick="submit_c('mail', 'remindmailconfirm'); return false" class="btn btn-success btn-block"><strong>リマインドメール送信</strong></button>
    </div>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-2" >
      <button onclick="submit_c('mail', 'surveymailconfirm'); return false" class="btn btn-info btn-block"><strong>アンケートメール送信</strong></button>
    </div>
    <br>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-3" >
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto "><strong>戻る</strong></button>
    </div>
  </form>
  <br>
</div>
{include file="PageFooter.tpl"}
{include file="PageEnd.tpl"}
