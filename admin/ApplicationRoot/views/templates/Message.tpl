<!--#####メッセージ出力ここから#####-->
{if $err_msg|@count != 0}
      <div class="alert alert-dismissable alert-danger"><i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><b>エラー</b></h4>
        {foreach from=$err_msg nofilter item=error}
        <p>[{$error.title}] {$error.message}</p>
        {/foreach}
      </div>
{elseif $info_msg != ''}
      <div class="alert alert-dismissable alert-success"><i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><b>完了</b></h4>
        <p>{$info_msg}</p>
      </div>
{/if}
<!--#####メッセージ出力ここまで#####-->
