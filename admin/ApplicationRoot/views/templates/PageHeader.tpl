<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SUITS OF THE YEAR 2019 管理</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link rel="stylesheet" href= "{$doc_root}css/bootstrap.min.css">
<link rel="stylesheet" href= "{$doc_root}css/style.css">
<link rel="stylesheet" href= "{$doc_root}css/font-awesome.min.css">
<!--[if lt IE 9]>
{literal}<style>.breadcrumb > li {float: left;}</style>{/literal}
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
