      <nav class="navbar navbar-expand-sm navbar-dark bg-dark" role="navigation">
        <a class="navbar-brand" href="#"><strong>メニュー</strong></a>
        <div class="collapse navbar-collapse justify-content-center">
          <ul class="navbar-nav">
            <li class="nav-item {if $nav=='entry_list'}active{/if}">
              <a class="nav-link" href="#" onclick="submit_c('top', 'index'); return false" class="btn btn-block  large-btn mx-auto">応募者一覧</a>
            </li>
            <li class="nav-item {if $nav=='winnerup'}active{/if}">
              {if $smarty.session.user_id == "goodbee"}
              <a class="nav-link" href="#" onclick="submit_c('up', 'winnerup'); return false" class="btn btn-block  large-btn mx-auto">当選者リストUP</a>
              {else}
              <a class="nav-link" class="btn btn-block  large-btn mx-auto">当選者リストUP</a>
              {/if}
            </li>
            <li class="nav-item {if $nav=='visitorsup'}active{/if}">
              {if $smarty.session.user_id == "goodbee"}
              <a class="nav-link" href="#" onclick="submit_c('up', 'visitorsup'); return false" class="btn btn-block  large-btn mx-auto">来場者リストUP</a>
              {else}
              <a class="nav-link" class="btn btn-block  large-btn mx-auto">来場者リストUP</a>
              {/if}
            </li>
            <li class="nav-item {if $nav=='mailtop'}active{/if}">
              {if $smarty.session.user_id == "goodbee"}
              <a class="nav-link" href="#" onclick="submit_c('mail', 'mailtop'); return false" class="btn btn-block  large-btn mx-auto">メール送信</a>
              {else}
              <a class="nav-link" class="btn btn-block  large-btn mx-auto">メール送信</a>
              {/if}
            </li>
            </ul>
          </div>
      </nav>
