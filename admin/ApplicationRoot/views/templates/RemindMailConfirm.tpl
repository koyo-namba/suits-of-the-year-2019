{include file="PageHeader.tpl"}
<body>
<!--header-->
{include file="Message.tpl"}
<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      {include file="PageNavi.tpl" nav="mailtop"}
      <div class="py-2">
      </div>  
      <div class="col-md-12 d-flex justify-content-center py-5">
        <h2 class="d-inline-block position-relative title">リマインドメール送信</h2>
      </div>
      <p class="text-center pb-5">以下の方々にリマインドメールを送信してもよろしいですか？</p>
      <br>
      <div class="col-md-5 mx-auto my-3 order-md-1">
      <button onclick="submit_c_Confirm('mail', 'remindmail'); return false" class="btn btn-block btn-outline-danger large-btn mx-auto ">送信する</button>
      </div>
      <br>
      <div class="col-md-5 mx-auto my-3 order-md-1">
      <button onclick="submit_c('mail', 'mailtop'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto ">戻る</button>
      </div>
      <br>
      <div class="col-50 ml-3">
      <table class="table table-striped small">
        <thead>
          <tr>
            <th>氏名</th>
            <th>mail</th>
          <tr>
        </thead>
        {for $i=1 to $counts}
        {if $winner[$i]['winners_flag'] == "1"}
        <tbody>
          <tr>
            <td align="left">{$winner[$i]["kanji_sei"]}{$winner[$i]["kanji_mei"]}</td>
            <td align="left">{$winner[$i]["email"]}</td>
          </tr> 
        </tbody>
        {/if}
        {/for}
      </table>
    </div>
    </div>
  </div>
</div>
</body>
</form>
</html>

</div>
{include file="PageFooter.tpl"}
{include file="PageEnd.tpl"}
