{include file="PageHeader.tpl"}
<body>
<!--header-->
{include file="Message.tpl"}
<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      {include file="PageNavi.tpl" nav="entry_list"}
      <div class="py-2">
      </div>  
      <div class="col-md-12 d-flex justify-content-center">
        <h2 class="d-inline-block position-relative title">応募者一覧</h2>
      </div>
    <br>
    </div>
    </div>
    <div class="col-md-5 mx-auto my-3 order-md-0" >
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-primary btn-block "><strong>更新</strong></button>
    </div>

    <h3 class="d-inline-block">現在{$mass}人</h3>
    <br>
    <br>
    <div class="col-50 ml-3">
      <table class="table table-striped small">
        <thead>
          <tr>
            <th>応募日時</th>
            <th>申込フロー</th>
            <th>氏名</th>
            <th>年齢</th>
            <th>同伴者</th>
            <th>会社名</th>
            <th>都道府県</th>
            <th>E-mail</th>
            <th>当選者</th>
            <th>来場者</th>
          <tr>
        </thead>
        {foreach from=$entries key=k item=v}
        <tbody>
          <tr>
            <td align="left">{$v["create_date"]}</td>
            <td align="left">{if $v["referrer"] == 1}日経新聞{else}世界文化社{/if}</td>
            <td align="left">{$v["kanji_sei"]} {$v["kanji_mei"]}</td>
            <td align="center">{$v["person_age"]}</td>
            <td align="center">{$v["gest_age"]}</td>
            <td align="left">{$v["company"]}</td>
            <td align="center">{$v["prefecture"]}</td>
            <td align="left">{$v["email"]}</td>
            <td align="center">{$v["winners_flag"]}</td>
            <td align="center">{$v["visitors_flag"]}</td></tr>
        </tbody>
        {/foreach}
      </table>
    </div>
  </form>
  <br>
</div>
{include file="PageFooter.tpl"}
{include file="PageEnd.tpl"}
