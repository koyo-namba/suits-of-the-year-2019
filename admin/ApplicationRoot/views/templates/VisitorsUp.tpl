{include file="PageHeader.tpl"}
<body>
<!--header-->
{include file="Message.tpl"}
<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      {include file="PageNavi.tpl" nav="visitorsup"}
      <div class="py-2">
      </div>  
        <div class="col-md-12 d-flex justify-content-center">
          <h2 class="d-inline-block position-relative title">来場者アップロード画面</h2>
        </div>
      </div>  
    </div> 
    <div class="py-3">
      </div> 
      <div class="col-md-5 mx-auto my-8 order-md-0" >
        <form action="" enctype="multipart/form-data" method="post">
          <input name="visitors_upload" type="file" />
        </form>
        <br>
        <br>
        <div class="button_wrapper">
          <button onclick="submit_c_Confirm('up', 'visitorsflagup'); return false" class="btn btn-danger large-btn mx-auto btn-lg ">アップロード</button>
        </div>
      </div>
    </div>
    <br>
    <div class="col-md-5 mx-auto my-5 order-md-1" >
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto "><strong>戻る</strong></button>
    </div>
  </form>
</div>
{include file="PageFooter.tpl"}
{include file="PageEnd.tpl"}