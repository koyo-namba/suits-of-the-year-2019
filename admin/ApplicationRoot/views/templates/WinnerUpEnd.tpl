{include file="PageHeader.tpl"}
<body>
<!--header-->
{include file="Message.tpl"}
<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      {include file="PageNavi.tpl" nav="winnerup"}
      <div class="py-2">
      </div>  
      <div class="col-md-12 d-flex justify-content-center py-5">
        <h2 class="d-inline-block position-relative title">当選者リストUP完了</h2>
      </div>
      <p class="text-center pb-5">当選者リストのアップロードが完了しました。</p>
      <br>
      <div class="col-md-5 mx-auto my-3 order-md-1">
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto ">TOPに戻る</button>
    </div>
    </div>
  </div>
</div>
</body>
</form>
</html>

</div>
{include file="PageFooter.tpl"}
{include file="PageEnd.tpl"}
