<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-20 16:06:07
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\Top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12506461245d6e1d1924da90-18819491%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b84646080533e67a1c5f91984b2d6de624b214e' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\Top.tpl',
      1 => 1568898737,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12506461245d6e1d1924da90-18819491',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d6e1d192714c2_41302014',
  'variables' => 
  array (
    'mass' => 0,
    'entries' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d6e1d192714c2_41302014')) {function content_5d6e1d192714c2_41302014($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("PageHeader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<body>
<!--header-->
<?php echo $_smarty_tpl->getSubTemplate ("Message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      <?php echo $_smarty_tpl->getSubTemplate ("PageNavi.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('nav'=>"entry_list"), 0);?>

      <div class="py-2">
      </div>  
      <div class="col-md-12 d-flex justify-content-center">
        <h2 class="d-inline-block position-relative title">応募者一覧</h2>
      </div>
    <br>
    </div>
    </div>
    <div class="col-md-5 mx-auto my-3 order-md-0" >
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-primary btn-block "><strong>更新</strong></button>
    </div>

    <h3 class="d-inline-block">現在<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['mass']->value, ENT_QUOTES, 'UTF-8', true);?>
人</h3>
    <br>
    <br>
    <div class="col-50 ml-3">
      <table class="table table-striped small">
        <thead>
          <tr>
            <th>応募日時</th>
            <th>申込フロー</th>
            <th>氏名</th>
            <th>年齢</th>
            <th>同伴者</th>
            <th>会社名</th>
            <th>都道府県</th>
            <th>E-mail</th>
            <th>当選者</th>
            <th>来場者</th>
          <tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['entries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
        <tbody>
          <tr>
            <td align="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["create_date"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="left"><?php if ($_smarty_tpl->tpl_vars['v']->value["referrer"]==1){?>日経新聞<?php }else{ ?>世界文化社<?php }?></td>
            <td align="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["kanji_sei"], ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["kanji_mei"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["person_age"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["gest_age"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["company"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["prefecture"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["email"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["winners_flag"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["visitors_flag"], ENT_QUOTES, 'UTF-8', true);?>
</td></tr>
        </tbody>
        <?php } ?>
      </table>
    </div>
  </form>
  <br>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("PageFooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("PageEnd.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>