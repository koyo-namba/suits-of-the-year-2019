<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-03 16:53:38
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\Exception.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15392908945d6e1c02552b55-60032936%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1e8a6901c2c0bb2edca70025389c76915af3f684' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\Exception.tpl',
      1 => 1461642730,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15392908945d6e1c02552b55-60032936',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'messages' => 0,
    'msg' => 0,
    'smarty_debug_frag' => 0,
    'detail' => 0,
    'class' => 0,
    'trace' => 0,
    'stack' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d6e1c02579fc2_96341835',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d6e1c02579fc2_96341835')) {function content_5d6e1c02579fc2_96341835($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("PageHeader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('login'=>'login','title'=>'','menu'=>''), 0);?>


<body>
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
    <h1>例外エラー</h1>
    <div class="alert alert-error alert-block">
      <h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8', true);?>
</h4>
      <?php  $_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['msg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->key => $_smarty_tpl->tpl_vars['msg']->value){
$_smarty_tpl->tpl_vars['msg']->_loop = true;
?>
      <p><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</p>
      <?php } ?>
    </div>
    <p class="text-center">
      <a href="#" class="btn btn-primary" onclick="javascript:submit_c('login'); return false;"><i class="icon-remove icon-white"></i> ログアウト</a>
    </p>

    <?php if ($_smarty_tpl->tpl_vars['smarty_debug_frag']->value=='1'){?>
    <p>DETAIL:</p>
    <p><?php echo $_smarty_tpl->tpl_vars['detail']->value;?>
</p>
    <h1>stacktrace - <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['class']->value, ENT_QUOTES, 'UTF-8', true);?>
</h1>
    <table class="table table-bordered table-striped table-hover">
      <tbody>
        <?php  $_smarty_tpl->tpl_vars['stack'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stack']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['trace']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stack']->key => $_smarty_tpl->tpl_vars['stack']->value){
$_smarty_tpl->tpl_vars['stack']->_loop = true;
?>
        <tr>
          <th><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stack']->value['class'], ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stack']->value['type'], ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stack']->value['function'], ENT_QUOTES, 'UTF-8', true);?>
</th>
          <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stack']->value['file'], ENT_QUOTES, 'UTF-8', true);?>
:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stack']->value['line'], ENT_QUOTES, 'UTF-8', true);?>
</td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php }?>
  </div>
  
<?php echo $_smarty_tpl->getSubTemplate ("PageFooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('login'=>'login'), 0);?>
<?php }} ?>