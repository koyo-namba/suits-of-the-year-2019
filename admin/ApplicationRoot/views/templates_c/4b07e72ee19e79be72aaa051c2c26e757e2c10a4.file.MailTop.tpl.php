<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-25 13:17:20
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\MailTop.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8790707865d76f16fc73e05-74715650%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b07e72ee19e79be72aaa051c2c26e757e2c10a4' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\MailTop.tpl',
      1 => 1569384978,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8790707865d76f16fc73e05-74715650',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d76f16fc9d887_25276602',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d76f16fc9d887_25276602')) {function content_5d76f16fc9d887_25276602($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("PageHeader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<body>
<!--header-->
<?php echo $_smarty_tpl->getSubTemplate ("Message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      <?php echo $_smarty_tpl->getSubTemplate ("PageNavi.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('nav'=>"mailtop"), 0);?>

      <div class="py-2">
      </div>  
      <div class="col-md-12 d-flex justify-content-center">
        <h2 class="d-inline-block position-relative title">Mail送信画面</h2>
      </div>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-0" >
      <button onclick="submit_c('mail', 'winnermailconfirm'); return false" class="btn btn-warning btn-block"><strong>当選メール送信</strong></button>
    </div>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-1" >
      <button onclick="submit_c('mail', 'remindmailconfirm'); return false" class="btn btn-success btn-block"><strong>リマインドメール送信</strong></button>
    </div>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-2" >
      <button onclick="submit_c('mail', 'surveymailconfirm'); return false" class="btn btn-info btn-block"><strong>アンケートメール送信</strong></button>
    </div>
    <br>
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-3" >
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto "><strong>戻る</strong></button>
    </div>
  </form>
  <br>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("PageFooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("PageEnd.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>