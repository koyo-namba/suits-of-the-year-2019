<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-19 15:55:26
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\Login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11390549745d6e1bd7f22377-73775542%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5dd142661b95103131e59b231a1908f6f03cfc2b' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\Login.tpl',
      1 => 1568876123,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11390549745d6e1bd7f22377-73775542',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d6e1bd80059a7_04339723',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d6e1bd80059a7_04339723')) {function content_5d6e1bd80059a7_04339723($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("PageHeader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<body>
<!--header-->
<?php echo $_smarty_tpl->getSubTemplate ("Message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
      <div class="py-5">
      <div class="col-md-12 d-flex justify-content-center">
        <h2 class="d-inline-block position-relative title">LOGIN&nbsp;FORM</h2>
      </div>
      </div>  
      <div class="body bg-gray">
        <div class="form-group">
          <div class="col-md-12 d-flex justify-content-center">
            <div class="col-sm-6" >
              <input type="text" name="user_id" class="form-control" placeholder="USER&nbsp;ID" value="" />
            </div>
          </div>
        </div>
        <div class="col-md-12 d-flex justify-content-center">
          <div class="col-sm-6">
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="PASSWORD" />
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-lg-4 mx-auto my-5">
        <button onclick="submit_c('login', 'login'); return false" class="btn btn-primary btn-block">LOG&nbsp;IN</button>
      </div>
    </div>
  </div>
  </div>
  </form>
  <br>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("PageFooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("PageEnd.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>