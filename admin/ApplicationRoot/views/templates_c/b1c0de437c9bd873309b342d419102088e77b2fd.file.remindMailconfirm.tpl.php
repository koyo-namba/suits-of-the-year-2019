<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-25 13:28:41
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\remindMailconfirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3555817435d847ad5444c77-11753232%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b1c0de437c9bd873309b342d419102088e77b2fd' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\remindMailconfirm.tpl',
      1 => 1569385705,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3555817435d847ad5444c77-11753232',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d847ad545e197_95857452',
  'variables' => 
  array (
    'counts' => 0,
    'i' => 0,
    'winner' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d847ad545e197_95857452')) {function content_5d847ad545e197_95857452($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("PageHeader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<body>
<!--header-->
<?php echo $_smarty_tpl->getSubTemplate ("Message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      <?php echo $_smarty_tpl->getSubTemplate ("PageNavi.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('nav'=>"mailtop"), 0);?>

      <div class="py-2">
      </div>  
      <div class="col-md-12 d-flex justify-content-center py-5">
        <h2 class="d-inline-block position-relative title">リマインドメール送信</h2>
      </div>
      <p class="text-center pb-5">以下の方々にリマインドメールを送信してもよろしいですか？</p>
      <br>
      <div class="col-md-5 mx-auto my-3 order-md-1">
      <button onclick="submit_c_Confirm('mail', 'remindmail'); return false" class="btn btn-block btn-outline-danger large-btn mx-auto ">送信する</button>
      </div>
      <br>
      <div class="col-md-5 mx-auto my-3 order-md-1">
      <button onclick="submit_c('mail', 'mailtop'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto ">戻る</button>
      </div>
      <br>
      <div class="col-50 ml-3">
      <table class="table table-striped small">
        <thead>
          <tr>
            <th>氏名</th>
            <th>mail</th>
          <tr>
        </thead>
        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int)ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['counts']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['counts']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0){
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++){
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
        <?php if ($_smarty_tpl->tpl_vars['winner']->value[$_smarty_tpl->tpl_vars['i']->value]['winners_flag']=="1"){?>
        <tbody>
          <tr>
            <td align="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['winner']->value[$_smarty_tpl->tpl_vars['i']->value]["kanji_sei"], ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['winner']->value[$_smarty_tpl->tpl_vars['i']->value]["kanji_mei"], ENT_QUOTES, 'UTF-8', true);?>
</td>
            <td align="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['winner']->value[$_smarty_tpl->tpl_vars['i']->value]["email"], ENT_QUOTES, 'UTF-8', true);?>
</td>
          </tr> 
        </tbody>
        <?php }?>
        <?php }} ?>
      </table>
    </div>
    </div>
  </div>
</div>
</body>
</form>
</html>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("PageFooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("PageEnd.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>