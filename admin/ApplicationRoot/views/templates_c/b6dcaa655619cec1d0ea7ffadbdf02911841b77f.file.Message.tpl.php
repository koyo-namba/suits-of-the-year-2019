<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-03 16:52:56
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\Message.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12479332905d6e1bd8083aa3-89690615%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6dcaa655619cec1d0ea7ffadbdf02911841b77f' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\Message.tpl',
      1 => 1461643028,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12479332905d6e1bd8083aa3-89690615',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'err_msg' => 0,
    'error' => 0,
    'info_msg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d6e1bd808ca65_58882440',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d6e1bd808ca65_58882440')) {function content_5d6e1bd808ca65_58882440($_smarty_tpl) {?><!--#####メッセージ出力ここから#####-->
<?php if (count($_smarty_tpl->tpl_vars['err_msg']->value)!=0){?>
      <div class="alert alert-dismissable alert-danger"><i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><b>エラー</b></h4>
        <?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['err_msg']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
        <p>[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['error']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
] <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['error']->value['message'], ENT_QUOTES, 'UTF-8', true);?>
</p>
        <?php } ?>
      </div>
<?php }elseif($_smarty_tpl->tpl_vars['info_msg']->value!=''){?>
      <div class="alert alert-dismissable alert-success"><i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><b>完了</b></h4>
        <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_msg']->value, ENT_QUOTES, 'UTF-8', true);?>
</p>
      </div>
<?php }?>
<!--#####メッセージ出力ここまで#####-->
<?php }} ?>