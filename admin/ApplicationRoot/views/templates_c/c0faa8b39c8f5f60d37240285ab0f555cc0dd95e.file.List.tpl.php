<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-19 14:47:10
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7487495005d6e2409c412f0-96618015%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0faa8b39c8f5f60d37240285ab0f555cc0dd95e' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\List.tpl',
      1 => 1568872022,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7487495005d6e2409c412f0-96618015',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d6e2409c68221_52746767',
  'variables' => 
  array (
    'doc_root' => 0,
    'mass' => 0,
    'entries' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d6e2409c68221_52746767')) {function content_5d6e2409c68221_52746767($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("PageHeader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<body>
<!--header-->
<header class="py-3 bg-white shadow-sm">
  <h1 class="text-center h-100"><a href="#"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['doc_root']->value, ENT_QUOTES, 'UTF-8', true);?>
img/logo.png" alt="スーツオブザイヤー2019" class="img-fluid h-100"></a></h1>
</header>
<?php echo $_smarty_tpl->getSubTemplate ("Message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">


  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
      <div class="py-5">
      <div class="col-md-12 d-flex justify-content-center">
        <h2 class="d-inline-block position-relative title">応募者一覧</h2>
      </div>
      </div>  
    <br>
    <div class="col-md-5 mx-auto my-3 order-md-0" >
      <button onclick="submit_c('top', 'list'); return false" class="btn btn-primary btn-block "><strong>更新</strong></button>
    </div>
    <div class="col-md-5 mx-auto my-3 order-md-1">
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto "><strong>戻る</strong></button>
    </div>
    </div>
    </div>
    <br>
    <h3 class="d-inline-block">現在<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['mass']->value, ENT_QUOTES, 'UTF-8', true);?>
人</h3>
    <br>
    <br>
    <div class="col-50 ml-3">
      <table class="table table-striped">
        <thead>
          <tr><th>氏名</th><th>年齢</th><th>同伴者</th><th>会社名</th><th>都道府県</th><th>E-mail</th><th>当選者</th><th>来場者</th><tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['entries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value){
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
        <tbody>
          <tr><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["kanji_sei"], ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["kanji_mei"], ENT_QUOTES, 'UTF-8', true);?>
</td><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["person_age"], ENT_QUOTES, 'UTF-8', true);?>
</td><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["gest_age"], ENT_QUOTES, 'UTF-8', true);?>
</td><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["company"], ENT_QUOTES, 'UTF-8', true);?>
</td><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["prefecture"], ENT_QUOTES, 'UTF-8', true);?>
</td><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["email"], ENT_QUOTES, 'UTF-8', true);?>
</td><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["winners_flag"], ENT_QUOTES, 'UTF-8', true);?>
</td><td align="center"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['v']->value["visitors_flag"], ENT_QUOTES, 'UTF-8', true);?>
</td></tr>
        </tbody>
        <?php } ?>
      </table>
    </div>
  </form>
  <br>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("PageFooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("PageEnd.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>