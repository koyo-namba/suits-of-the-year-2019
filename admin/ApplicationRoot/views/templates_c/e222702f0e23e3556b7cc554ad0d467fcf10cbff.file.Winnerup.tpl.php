<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-25 13:17:19
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\Winnerup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5937085855d6e24ca9bad93-31867608%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e222702f0e23e3556b7cc554ad0d467fcf10cbff' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\Winnerup.tpl',
      1 => 1569384808,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5937085855d6e24ca9bad93-31867608',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d6e24ca9d4e91_36858908',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d6e24ca9d4e91_36858908')) {function content_5d6e24ca9d4e91_36858908($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("PageHeader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<body>
<!--header-->
<?php echo $_smarty_tpl->getSubTemplate ("Message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="form-box">
  <form name="mainform" id="mainform" method="post" action="" enctype="multipart/form-data">
  <div class="container">
  <div class="row">
    <div class="col-md-10 mx-auto">
        <div class="py-2">
      </div>  
      <?php echo $_smarty_tpl->getSubTemplate ("PageNavi.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('nav'=>"winnerup"), 0);?>

      <div class="py-2">
      </div>  
        <div class="col-md-12 d-flex justify-content-center">
          <h2 class="d-inline-block position-relative title">当選者アップロード画面</h2>
        </div>
      </div>  
    </div> 
    <div class="py-3">
      </div> 
      <div class="col-md-5 mx-auto my-8 order-md-0" >
        <form action="" enctype="multipart/form-data" method="post">
          <input name="winners_upload" type="file" />
        </form>
        <br>
        <br>
        <div class="button_wrapper">
          <button onclick="submit_c_Confirm('up', 'winnerflagup'); return false" class="btn btn-danger large-btn mx-auto btn-lg ">アップロード</button>
        </div>
      </div>
    </div>
    <br>
    <div class="col-md-5 mx-auto my-5 order-md-1" >
      <button onclick="submit_c('top', 'index'); return false" class="btn btn-block btn-outline-secondary large-btn mx-auto "><strong>戻る</strong></button>
    </div>
  </form>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("PageFooter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("PageEnd.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>