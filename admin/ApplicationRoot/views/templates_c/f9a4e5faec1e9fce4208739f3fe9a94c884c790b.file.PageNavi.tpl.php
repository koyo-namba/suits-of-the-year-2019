<?php /* Smarty version Smarty-3.1-DEV, created on 2019-09-25 13:27:29
         compiled from "D:\workspace\suitsoftheyear\admin\ApplicationRoot\views\templates\PageNavi.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3530894685d847a5f4aaad1-94020201%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f9a4e5faec1e9fce4208739f3fe9a94c884c790b' => 
    array (
      0 => 'D:\\workspace\\suitsoftheyear\\admin\\ApplicationRoot\\views\\templates\\PageNavi.tpl',
      1 => 1569385601,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3530894685d847a5f4aaad1-94020201',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_5d847a5f4fc367_29924642',
  'variables' => 
  array (
    'nav' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d847a5f4fc367_29924642')) {function content_5d847a5f4fc367_29924642($_smarty_tpl) {?>      <nav class="navbar navbar-expand-sm navbar-dark bg-dark" role="navigation">
        <a class="navbar-brand" href="#"><strong>メニュー</strong></a>
        <div class="collapse navbar-collapse justify-content-center">
          <ul class="navbar-nav">
            <li class="nav-item <?php if ($_smarty_tpl->tpl_vars['nav']->value=='entry_list'){?>active<?php }?>">
              <a class="nav-link" href="#" onclick="submit_c('top', 'index'); return false" class="btn btn-block  large-btn mx-auto">応募者一覧</a>
            </li>
            <li class="nav-item <?php if ($_smarty_tpl->tpl_vars['nav']->value=='winnerup'){?>active<?php }?>">
              <?php if ($_SESSION['user_id']=="goodbee"){?>
              <a class="nav-link" href="#" onclick="submit_c('up', 'winnerup'); return false" class="btn btn-block  large-btn mx-auto">当選者リストUP</a>
              <?php }else{ ?>
              <a class="nav-link" class="btn btn-block  large-btn mx-auto">当選者リストUP</a>
              <?php }?>
            </li>
            <li class="nav-item <?php if ($_smarty_tpl->tpl_vars['nav']->value=='visitorsup'){?>active<?php }?>">
              <?php if ($_SESSION['user_id']=="goodbee"){?>
              <a class="nav-link" href="#" onclick="submit_c('up', 'visitorsup'); return false" class="btn btn-block  large-btn mx-auto">来場者リストUP</a>
              <?php }else{ ?>
              <a class="nav-link" class="btn btn-block  large-btn mx-auto">来場者リストUP</a>
              <?php }?>
            </li>
            <li class="nav-item <?php if ($_smarty_tpl->tpl_vars['nav']->value=='mailtop'){?>active<?php }?>">
              <?php if ($_SESSION['user_id']=="goodbee"){?>
              <a class="nav-link" href="#" onclick="submit_c('mail', 'mailtop'); return false" class="btn btn-block  large-btn mx-auto">メール送信</a>
              <?php }else{ ?>
              <a class="nav-link" class="btn btn-block  large-btn mx-auto">メール送信</a>
              <?php }?>
            </li>
            </ul>
          </div>
      </nav>
<?php }} ?>