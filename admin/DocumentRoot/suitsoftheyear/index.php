<?php

// おきまりっぽい
error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 0);
date_default_timezone_set('Asia/Tokyo');

/*
 * index.phpで以下の事を行う
 * ①以下３つのパス定義
 * 　　PROJECT_ROOT、APP_ROOT、DOC_ROOT
 * ②共通Libへのパスを、phpのインクルードパスに追加x\z
 * 　　※パス自体は、app.iniより取得する
 * ③Loggerの初期化
 *
 * その後、フロントコントローラのセットアップ
 */

// ① set project_root
define('PROJECT_ROOT', realpath(dirname(__FILE__).'/../../'));
//define('DOC_ROOT', realpath(dirname(__FILE__).'/../'));
define('DOC_ROOT', 'D:/workspace/suitsoftheyear/DocumentRoot/');
define('APP_ROOT', PROJECT_ROOT.'/ApplicationRoot/');

// ② add COMMON_LIB_DIR to include_path
require_once APP_ROOT.'/controllers/helper/AppConfig.php';
AppConfig::initialize(APP_ROOT.'/configs/app.ini', 'app');
set_include_path(get_include_path().PATH_SEPARATOR.AppConfig::get('common_lib_path'));

// ③ logger
define('LOG4PHP_CONFIGURATION', APP_ROOT.'/configs/log4php.properties');
require_once 'Logger.php';
Logger::configure(LOG4PHP_CONFIGURATION);

// ④ setup controller
require_once 'Zend/Controller/Front.php';
require_once 'Zend/Controller/Router/Route.php';
require_once APP_ROOT.'/filter/RequestFilter.php';

$front = Zend_Controller_Front::getInstance();
$front->setControllerDirectory(APP_ROOT.'/controllers');

// dispatch root-access : http://.../app/ -> /app/login/index
$router = $front->getRouter();
$route = new Zend_Controller_Router_Route(
	'', array('controller' => 'login', 'action' => 'index')
);
$router->addRoute('', $route);

// use viewRender
$front->setParam('noViewRenderer', true);

// set filter
$front->registerPlugin(new Filter_RequestFilter());

$front->dispatch();


?>
