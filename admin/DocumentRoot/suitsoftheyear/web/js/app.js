function delHyphen(obj)
{
	var val = obj.value;
	obj.value = val.split('-').join('');
}

/**
 *  ページング
 */
function paging(action, page_no)
{
	submit_a(action,'page_no',page_no);
}

/**
 * form内容の全クリア
 */
function clear_form()
{
	for(var i=0; i<document.mainform.elements.length; i++) {
       	document.mainform.elements[i].value = "";
    }
}

function open_window(controller, action, args) {
	var temp = new Array("/" + g_site_name);
	for(var i=0; i<arguments.length; i++)
		temp.push(arguments[i]);
	
//	if (isLocked())
//		return false;
//	setLock();

//	document.forms['mainform'].target = "_blank";
//	document.forms['mainform'].action = temp.join("/");
//	document.forms['mainform'].submit();
	window.open(temp.join("/"), "barcode","fullscreen=yes");
}

/**
 * コントロール名およびアクション名指定でサブミットする
 * 引数：必須 <コントローラ名>, <アクション名>
 *       オプション <パラメータ名１>, <パラメータ値１>, <パラメータ名２>, <パラメータ値２>, ...
 *       ※可変長引数。名前と値のペアで、いくつでも設定可能
 * ex:
 *  onClick="submit_c('account-search', 'select');return false"
 *  onClick="submit_c('account-search', 'select', 'page_no', '3');return false"
 *
 * オプションで指定したパラメータは、actionメソッド内で↓で取り出す。
 * 　$page_no = $this-->_getParam[page_no]; // value is '3'
 *
 */
function submit_c(controller, action, args) {
	var temp = new Array("/" + g_site_name);
	for(var i=0; i<arguments.length; i++)
		temp.push(arguments[i]);
	mainform_submit(temp.join("/"));
}

function submit_c_Confirm(controller, action, args) {
	if (confirm('処理を実行してもよろしいですか？')) {
		var temp = new Array("/" + g_site_name);
		for(var i=0; i<arguments.length; i++)
			temp.push(arguments[i]);
		mainform_submit(temp.join("/"));
	}
}

function submit_c_delConfirm(controller, action, args) {
	if (confirm('本当に削除してもよろしいですか？')) {
		var temp = new Array("/" + g_site_name);
		for(var i=0; i<arguments.length; i++)
			temp.push(arguments[i]);
		mainform_submit(temp.join("/"));
	}
}

function submit_c_Message(controller, action, args)
{
	if (confirm(arguments[arguments.length-1])) {
		var temp = new Array("/" + g_site_name);
		for(var i=0; i<arguments.length-1; i++)
			temp.push(arguments[i]);
		mainform_submit(temp.join("/"));
	}
}


/**
 * submit_cのコントロール名省略版。他は全て同じ
 */
function submit_a(action, args) {
	var temp = new Array("/" + g_site_name + "/" + g_controllerName);
	for(var i=0; i<arguments.length; i++)
		temp.push(arguments[i]);
	mainform_submit(temp.join("/"));
}

function submit_a_Confirm(action, args)
{
	if (confirm('処理を実行してもよろしいですか？')) {
		var temp = new Array("/" + g_site_name + "/" + g_controllerName);
		for(var i=0; i<arguments.length; i++)
			temp.push(arguments[i]);
		mainform_submit(temp.join("/"));
	}
}

function submit_a_delConfirm(action, args)
{
	if (confirm('本当に削除してもよろしいですか？')) {
		var temp = new Array("/" + g_site_name + "/" + g_controllerName);
		for(var i=0; i<arguments.length; i++)
			temp.push(arguments[i]);
		mainform_submit(temp.join("/"));
	}
}

function submit_c_Blank(controller, action, args) {
	var temp = new Array("/" + g_site_name);
	for(var i=0; i<arguments.length; i++)
		temp.push(arguments[i]);
	mainform_submit_blank(temp.join("/"));
}

function mainform_submit_blank(url) {
//	if (isLocked())
//		return false;
//	setLock();

	document.forms['mainform'].target = "_blank";
	document.forms['mainform'].action = url;
	document.forms['mainform'].submit();
}

function mainform_submit(url) {
//	if (isLocked())
//		return false;
//	setLock();

	document.forms['mainform'].target = "_self";
	document.forms['mainform'].action = url;
	document.forms['mainform'].submit();
}

var submit_lock = false;

function isLocked() {
	if (submit_lock) alert('ただ今処理中につき、「OK」ボタンを押してお待ち下さい。');
	return submit_lock;
}

function setLock() {
	submit_lock = true;
	setTimeout("releaseLock()",5000); // 5秒後解放する
}

function releaseLock() {
	submit_lock = false;
}

var KEYCODE_ENTER = 13;
function enterkey_submit_c(event, controll, action) {
	if (event.keyCode == KEYCODE_ENTER)
		submit_c(controll,action);
}

function bodyOnload() {
	// overload
}

// 基本submit_aと一緒
function previewPdf(action, args)
{
	var temp = new Array("/" + g_site_name + "/" + g_controllerName);
	for(var i=0; i<arguments.length; i++)
		temp.push(arguments[i]);
	
	document.forms['mainform'].target = "_blank";
	document.forms['mainform'].action = temp.join("/");
	document.forms['mainform'].submit();
}

function toOneByte(obj){
	var str=obj.value;
	obj.value = str.toOneByteAlphaNumeric();
	return false;
}

String.prototype.toOneByteAlphaNumeric = function(){
	return this.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
		return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
	});
}


