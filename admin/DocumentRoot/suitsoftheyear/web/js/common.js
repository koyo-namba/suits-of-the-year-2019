(function($) {
  $(function() {
    var $header = $('#header');
    $('#nav-toggle').click(function() {
      $header.toggleClass('open');
    });
  });
})(jQuery);
